package inscricao.faces.mngbeans;

import inscricao.entity.Candidato;
import inscricao.entity.Idioma;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.model.SelectItem;
import utfpr.faces.support.PageBean;

/**
 *
 * @author Wilson
 */
@ManagedBean
@ApplicationScoped
public class RegistroBean extends PageBean {
    
    public ArrayList<Candidato> candidatosList ;
    
    public RegistroBean() {
        candidatosList = new ArrayList<>();
    }

    public void setCandidatosList(ArrayList<Candidato> CandidatosList) {
        this.candidatosList = CandidatosList;
    }

    public ArrayList<Candidato> getCandidatosList() {
        return candidatosList;
    }
    
    
    

    
    
}
